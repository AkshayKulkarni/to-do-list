import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../displaytask/displaytask.component';
import { TaskService } from '../addtask/task.service';
import { DeletetaskService } from './deletetask.service';
import { Task } from '../models/task.model';

@Component({
  selector: 'app-confirmationmodal',
  templateUrl: './confirmationmodal.component.html',
  styleUrls: ['./confirmationmodal.component.css']
})
export class ConfirmationmodalComponent implements OnInit {

  // tslint:disable-next-line: max-line-length
  constructor(public dialogRef: MatDialogRef<ConfirmationmodalComponent>, @Inject(MAT_DIALOG_DATA) public data: Task, public deletetaskservice: DeletetaskService) { }
  deleteTaskid: number;
  dismiss() {
    this.dialogRef.close();
  }
  onNoClick(): void {
    // cancle dialog
    this.dialogRef.close();
  }
  ngOnInit() {
    // get taskid
   // this.deleteTaskid = this.data.taskid;
  }
  deleteaccept(taskObj) {
    // call delete specfic using service call
    this.deletetaskservice.deleteTask(taskObj).subscribe(deleteresult => console.log('delete successfully', deleteresult));
  }

}
