import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Task } from '../models/task.model';
@Injectable({
  providedIn: 'root'
})
export class DeletetaskService {

  constructor(private http: HttpClient) { }
  deleteTaskid: number;
  baseUrl = 'https://api.todoist.com/rest/v1/tasks';
  token = 'Bearer ' + '8ed8258b5f386897221b8f0511ce20d9e1c6bfc7';
  httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': this.token
      })
    };
    // deleteTask(taskid: number): Observable<Task> {
    //   // Delete task name on server
    //   this.deleteTaskid = taskid;
    //   return this.http.delete<Task>(this.baseUrl + '/' + this.deleteTaskid + '/',  this.httpOptions);
    // }
    deleteTask(taskObj): Observable<Task> {
      // Delete task name on server
    //  this.deleteTaskid = taskid;
      return this.http.delete<Task>(this.baseUrl + '/' + taskObj.id + '/',  this.httpOptions);
    }
}
