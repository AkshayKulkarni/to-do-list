import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Task } from '../models/task.model';

@Injectable({
  providedIn: 'root'
})
export class RegisterserviceService {
  constructor(private http: HttpClient) { }
  baseUrl = 'http://192.168.0.132:5000';
  registerSignup(registerFormValues): Observable<any> {
    return  this.http.post<Task>(this.baseUrl + '/signup', registerFormValues);
 }
 // getdata() { return  this.http.get<Userdata[]>(this.baseUrl + '/viewtask'); }
  // loginuser(user): Observable<any> { return  this.http.post<any>(this.baseUrl + '/login', user); }
  // taskaddtoserver(task): Observable<any> { return  this.http.post<any>(this.baseUrl + '/addtask', task); }
}
