import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Task } from '../models/task.model';
import { RegisterserviceService } from './registerservice.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private rr: Router, private registerService: RegisterserviceService) { }
  registerForm: FormGroup;
  username: string;
  emailid: string;
  password: string;
  registerFormValues;
  onclick() { this.rr.navigate(['/Login']); }
  SignUp() {
  this.registerFormValues = (this.registerForm.value);
  this.registerService.registerSignup(this.registerFormValues).subscribe (x => console.log('insert successfully'));
  }
  ngOnInit() {
    this.registerForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      emailid: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z0-9]+@[a-zA-Z]+[.][a-zA-Z]+')]),
      password: new FormControl('', [Validators.required]),
      confirmPassword : new FormControl('', [Validators.required]),
    });
  }

}
