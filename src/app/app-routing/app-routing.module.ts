import { NgModule } from '@angular/core';
      import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../login/login.component';
import { RegisterComponent } from '../register/register.component';
import { ForgetpasswordComponent } from '../forgetpassword/forgetpassword.component';
import { DisplaytaskComponent } from '../displaytask/displaytask.component';
import { EdittaskComponent } from '../edittask/edittask.component';
import { PagenotfoundComponent } from '../pagenotfound/pagenotfound.component';
const routes: Routes = [
  {path: '', redirectTo: '/DisplayTask', pathMatch: 'full'},
  {path: 'Login', component: LoginComponent},
  {path: 'Register', component: RegisterComponent},
  {path: 'Forgetpassword', component: ForgetpasswordComponent},
  {path: 'DisplayTask', component: DisplaytaskComponent},
  {path: 'Edittask', component: EdittaskComponent},
  {path: '**', component: PagenotfoundComponent}
];
@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
