import { Component, OnInit, ViewChild } from '@angular/core';
import { Tasks } from '../models/tasks.model';
import {Task} from '../models/task.model';
import { MatDialog } from '@angular/material/dialog';
import { AddtaskComponent } from '../addtask/addtask.component';
import { Router } from '@angular/router';
import { EdittaskComponent } from '../edittask/edittask.component';
import { ConfirmationmodalComponent } from '../confirmationmodal/confirmationmodal.component';
import { CompletedtaskComponent } from '../completedtask/completedtask.component';
import { DisplaytasksService } from './displaytasks.service';
import { MatPaginator } from '@angular/material/paginator';

export interface DialogData {
  taskid: number;
}

@Component({
  selector: 'app-displaytask',
  templateUrl: './displaytask.component.html',
  styleUrls: ['./displaytask.component.css']
})
export class DisplaytaskComponent implements OnInit {
  // tslint:disable-next-line: max-line-length
  config: any;
  // tslint:disable-next-line: max-line-length
  constructor(private displayTaskService: DisplaytasksService, public  addTaskopendialog: MatDialog, private rr: Router, public editTaskdialog: MatDialog, public deleteTaskdialog: MatDialog, public CompletedTaskdialog: MatDialog) { }
  // Tasks: Task = new Task();
  // Taskclass[];
  // Tasks = [];
  public a;
  public tasks: Tasks;
  myModel: boolean;
  taskid: number;
  collection = { count: 60, data: [] };
  // selectAll() {
  //   this.myModel = true;
  // }
  ngOnInit() {
    // get Alltask list from service
   // this.myModel = false;
  //  this.displayTaskService.getAllTasks().subscribe((data) => {
     // console.log(data);
    //  this.Tasks = data;
    //  });
    this.displayTaskService.getAllTasks().subscribe(tasks => {
      this.tasks = tasks;
      // console.log(this.tasks);
    });
  }

  AddTaskopenDialog(): void {
    const addTaskdialogRef = this. addTaskopendialog.open(AddtaskComponent, {
      width: '500px',
    });
   // dialogRef.afterClosed();
    addTaskdialogRef.disableClose = true;
  }
  // EditTaskopenDialog(tasknumber: number): void {
  //   // Edittask Dialog
  //    this.taskid = tasknumber;
  //    const editTaskdialogRef = this.editTaskdialog.open(EdittaskComponent, {
  //     width: '500px',
  //     data: {taskid: this.taskid}
  //   });
  //    editTaskdialogRef.disableClose = true;
  //   // dialogRef1.afterClosed();
  // }
  EditTaskopenDialog(task): void {
    // Edittask Dialog
    // console.log(task);
    const editTaskdialogRef = this.editTaskdialog.open(EdittaskComponent, {
      width: '500px',
      data: task
    });
    editTaskdialogRef.disableClose = true;
    // dialogRef1.afterClosed();
  }
  // DeleteTaskopenDialog(tasknumber: number): void {
  //   // Confirmation Dialog
  //   this.taskid = tasknumber;
  //   const deleteTaskdialogRef = this.deleteTaskdialog.open(ConfirmationmodalComponent, {
  //     width: '500px',
  //     data: {taskid: this.taskid}
  //   });
  //   // dialogRef2.afterClosed();
  //   deleteTaskdialogRef.disableClose = true;
  // }
  DeleteTaskopenDialog(task): void {
    // Confirmation Dialog
    // this.taskid = tasknumber;
    const deleteTaskdialogRef = this.deleteTaskdialog.open(ConfirmationmodalComponent, {
      width: '500px',
      data: task
    });
    // dialogRef2.afterClosed();
    deleteTaskdialogRef.disableClose = true;
  }
  CompletedtaskopenDialog(): void {
    // Complete Task Dialog
    const completedTaskdialogRef = this.CompletedTaskdialog.open(CompletedtaskComponent, {
      width: '500px',
    });
    // dialogRef3.afterClosed();
    completedTaskdialogRef.disableClose = true;
  }
  logout() {
    // localStorage.removeItem('Authorization');
    this.rr.navigate(['/Login']);
  }
  // onChange(tasknumber: number) {
  //   this.taskid = tasknumber;
  //   this.displayTaskService.closeTask(this.taskid).subscribe(result => console.log('close successfully', result));
  // }
  onChange(task) {
    // this.taskid = tasknumber;
    this.displayTaskService.closeTask(task).subscribe(result => console.log('close successfully', result));
  }

}
