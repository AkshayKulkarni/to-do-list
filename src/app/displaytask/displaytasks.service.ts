import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable} from 'rxjs';
import { Tasks } from '../models/tasks.model';
import {map} from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class DisplaytasksService {

  constructor(private http: HttpClient) { }
  closeTaskid: number;
  baseUrl = 'https://api.todoist.com/rest/v1/tasks';
  token = 'Bearer ' + '8ed8258b5f386897221b8f0511ce20d9e1c6bfc7';
  httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': this.token
      })
    };
  getAllTasks() {
        // Fetch All Task Details
      return this.http.get<any>(this.baseUrl , this.httpOptions).pipe(map((res) => new Tasks().deserialize(res)));
    }
  closeTask(taskObj): Observable<Tasks> {
    // Completed Task
    // this.closeTaskid = taskId;
   //  this.token = 'Bearer ' + '8ed8258b5f386897221b8f0511ce20d9e1c6bfc7';
    // let headers = new HttpHeaders();
    // headers = headers.set('Content-Type', 'application/json;').set('Authorization', this.token);
    return this.http.post<Tasks>(this.baseUrl + '/' + taskObj.id + '/close' + '/', {}, this.httpOptions);
  }
}
