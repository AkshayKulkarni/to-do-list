import { Deserializable } from './deserializable.model';
import { jsonpCallbackContext } from '@angular/common/http/src/module';

export class Task implements Deserializable<Task> {
    private _content: string;
    private _id: number;
    // tslint:disable-next-line: max-line-length
   // constructor(private content?: string, public taskid?: number) {
        // this.username = username;
        // this.emailid  = emailid;
        // this.password = password;
        // this.content  = content;
        // this.taskid   = taskid;
   // }
    public get content() {
        return this._content;
    }
    public set content(content: string) {
        this._content = content;
    }
    public get id() {
        return this._id;
    }
    public set id(taskid: number) {
        this._id = taskid;
    }
    serialize() {
        return {content: this.content};
    }
    deserialize(input: any): Task {
        return Object.assign(this, input);
    }
}
