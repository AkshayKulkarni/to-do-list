import { Deserializable } from './deserializable.model';
import {Task} from './task.model';

export class Tasks implements Deserializable<Tasks> {
    private _tasks: Task[];

    deserialize(input: any): Tasks {
        this._tasks = input.map(
            task => new Task().deserialize(task)
        );
        return this;
    }

    public get tasks(): Task[] {
        return this._tasks;
    }

    public set tasks(value: Task[]) {
        this._tasks = value;
    }
}
