import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-completedtask',
  templateUrl: './completedtask.component.html',
  styleUrls: ['./completedtask.component.css']
})
export class CompletedtaskComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<CompletedtaskComponent>) { }

  ngOnInit() {
  }

}
