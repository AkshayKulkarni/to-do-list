import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TaskService } from '../addtask/task.service';
import { DialogData } from '../displaytask/displaytask.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Task} from '../models/task.model';
import { EdittaskService } from './edittask.service';

@Component({
  selector: 'app-edittask',
  templateUrl: './edittask.component.html',
  styleUrls: ['./edittask.component.css']
})
export class EdittaskComponent implements OnInit {
  updateTaskform: FormGroup;
  // tslint:disable-next-line: max-line-length
  constructor(private rr: Router, public dialogRef: MatDialogRef<EdittaskComponent>, private editTaskService: EdittaskService, public dialogRef1: MatDialogRef<EdittaskComponent>, @Inject(MAT_DIALOG_DATA) public data: Task) { }
  id: number;
  updateTaskid: number;
  task: string;
  taskResult: Task = new Task();
  content: string;
  // updatetaskformvalues;
  ngOnInit() {
    this.updateTaskform = new FormGroup({
      content: new FormControl('', [Validators.required])
    });
    // this.updateTaskid = this.data.taskid;
    // this.editTaskService.getTaskById(this.updateTaskid).subscribe(result => { this.taskResult = result; });
  }
  updatetask() {
    // tslint:disable-next-line: max-line-length
    this.editTaskService.updateTask(this.data).subscribe (taskResult => {console.log('Task name update successfully');  } );
  }
}
