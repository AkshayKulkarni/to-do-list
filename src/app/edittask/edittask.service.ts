import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { Task } from '../models/task.model';

@Injectable({
  providedIn: 'root'
})
export class EdittaskService {
  constructor(private http: HttpClient) { }
  taskById: number;
  updateTaskId: number;
  baseUrl = 'https://api.todoist.com/rest/v1/tasks';
  token = 'Bearer ' + '8ed8258b5f386897221b8f0511ce20d9e1c6bfc7';
  httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': this.token
      })
    };
    getTaskById(taskid: number) {
      // Get Task id from server
      this.taskById = taskid;
      return this.http.get<Task>(this.baseUrl + '/' + this.taskById, this.httpOptions);
    }
    updateTask(taskObj): Observable<Task>  {
      // Update task name on server
      const taskdata = taskObj.serialize();
      console.log('editservice' + taskdata);
     // this.updateTaskId = taskid;
      return this.http.post<Task>(this.baseUrl + '/' + taskObj.id, taskdata, this.httpOptions);
    }
}
