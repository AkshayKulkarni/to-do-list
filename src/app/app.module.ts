import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DisplaytaskComponent } from './displaytask/displaytask.component';
import { RegisterserviceService } from './register/registerservice.service';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { MaterialModule } from './material/material.module';
import { AddtaskComponent } from './addtask/addtask.component';
import { LoginService } from './login/login.service';
import { TaskService } from './addtask/task.service';
import { EdittaskComponent } from './edittask/edittask.component';
import { TokenInterceptorService } from './token-interceptor.service';
import { ViewtaskComponent } from './viewtask/viewtask.component';
import { ConfirmationmodalComponent } from './confirmationmodal/confirmationmodal.component';
import { CompletedtaskComponent } from './completedtask/completedtask.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    ForgetpasswordComponent,
    DisplaytaskComponent,
    AddtaskComponent,
    EdittaskComponent,
    ViewtaskComponent,
    ConfirmationmodalComponent,
    CompletedtaskComponent,
    PagenotfoundComponent,
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule, MaterialModule
    // tslint:disable-next-line: whitespace
    , ReactiveFormsModule, FormsModule, HttpClientModule, AppRoutingModule, NgxPaginationModule, Ng2SearchPipeModule
  ],
  entryComponents: [AddtaskComponent, ViewtaskComponent, ConfirmationmodalComponent, CompletedtaskComponent
  ],
  providers: [RegisterserviceService, LoginService, TaskService],
  bootstrap: [AppComponent]
})
export class AppModule { }
