import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Task } from '../models/task.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(private http: HttpClient) { }
baseUrl = 'https://api.todoist.com/rest/v1/projects';
token = 'Bearer ' + '8ed8258b5f386897221b8f0511ce20d9e1c6bfc7';
// loginuser(user): Observable<any> { return  this.http.post<any>(this.baseUrl + '/login', user); }
userLogin(userFormValues): Observable<any> {
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      // tslint:disable-next-line: object-literal-key-quotes
      'Authorization': this.token
    })
  };
  return  this.http.post<any>(this.baseUrl , userFormValues, httpOptions);
}
getToken() {
    return localStorage.getItem('authorization');
  }
}
