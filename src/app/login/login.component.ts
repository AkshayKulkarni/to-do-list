import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {HttpHeaders, HttpResponse} from '@angular/common/http';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  username: string;
  emailid: string;
  password: string;
  loginFormValues;
  authorizationres;
  token: string;
  constructor(private rr: Router, private loginService: LoginService) {}
  Login() {
    this.loginFormValues = (this.loginForm.value);
    this.loginService.userLogin(this.loginFormValues).subscribe ( (res: any[]) => {
      console.log(res);
      this.authorizationres = res;
      // tslint:disable-next-line: no-string-literal
      localStorage.setItem('authorization', res ['authorization']);
      this.token = localStorage.getItem('authorization');
    },
    err => {console.log(err); }
    );
    if (this.authorizationres !== ' ') {
      this.rr.navigate(['/Displayuser']);
    }
    alert('data send successfully');
  }
  onclick() {
    this.rr.navigate(['/Register']);
  }
  onforgotpassword() {
    this.rr.navigate(['/Forgetpassword']);
  }
  ngOnInit() {
    this.loginForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });
  }
}
