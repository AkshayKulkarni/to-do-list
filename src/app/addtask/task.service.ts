import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Task } from '../models/task.model';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  constructor(private http: HttpClient) { }
  closeid: number;
  deleteid: number;
  taskbyid: number;
  updatetaskid: number;
  // baseUrl = 'http://192.168.0.132:5000';
  baseUrl = 'https://api.todoist.com/rest/v1/tasks';
  token = 'Bearer ' + '8ed8258b5f386897221b8f0511ce20d9e1c6bfc7';
  httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': this.token
      })
    };
addTask(taskFormsvalues): Observable<Task> {
  // Task name Add on server
    return  this.http.post<Task>(this.baseUrl, taskFormsvalues, this.httpOptions);
  }
// getAllTasks() {
//   // Fetch All Task Details
//     return this.http.get<Task>(this.baseUrl , this.httpOptions);
//  }
// getTaskById(taskid: number) {
//   // Get Task id from server
//   this.taskbyid = taskid;
//   return this.http.get<Task>(this.baseUrl + '/' + this.taskbyid, this.httpOptions);
// }
// updateTask(taskid, task): Observable<Task>  {
//   // Update task name on server
//   this.updatetaskid = taskid;
//   return this.http.post<Task>(this.baseUrl + '/' + this.updatetaskid, task, this.httpOptions);
//  }

// deleteTask(taskid: number): Observable<Task> {
//   // Delete task name on server
//   this.deleteid = taskid;
//   return this.http.delete<Task>(this.baseUrl + '/' + this.deleteid + '/',  this.httpOptions);
// }
// closeTask(taskid: number): Observable<Task> {
//   // Completed Task
//   this.closeid = taskid;
//   this.token = 'Bearer ' + '8ed8258b5f386897221b8f0511ce20d9e1c6bfc7';
//   // let headers = new HttpHeaders();
//   // headers = headers.set('Content-Type', 'application/json;').set('Authorization', this.token);
//   return this.http.post<Task>(this.baseUrl + '/' + this.closeid + '/close' + '/', {}, this.httpOptions);
// }

}
