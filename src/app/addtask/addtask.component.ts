import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TaskService } from './task.service';
import { ViewtaskComponent } from '../viewtask/viewtask.component';
@Component({
  selector: 'app-addtask',
  templateUrl: './addtask.component.html',
  styleUrls: ['./addtask.component.css']
})
export class AddtaskComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<ViewtaskComponent>, private addTaskService: TaskService) { }
  addTaskForm: FormGroup;
  addTaskFormValues: string;
  Addtask() {
    this.addTaskFormValues = (this.addTaskForm.value );
    // this.taskservice.taskaddtoserver(this.addtaskformvalues).subscribe (x => console.log('insert successfully'));
    this.addTaskService.addTask(this.addTaskFormValues).subscribe (data => {console.log(data);
      // tslint:disable-next-line: align
      { if (data) { alert('insert successfully'); window.location.reload(); }  } }
    );
  }
  ngOnInit() {
    // validation for task name
    this.addTaskForm = new FormGroup({
      content: new FormControl('', [Validators.required]),
    });
  }

}
